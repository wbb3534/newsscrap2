package com.seop.newsscrap2.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
