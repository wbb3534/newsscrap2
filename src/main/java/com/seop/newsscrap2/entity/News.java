package com.seop.newsscrap2.entity;

import com.seop.newsscrap2.interfaces.CommonModelBuilder;
import com.seop.newsscrap2.model.ScrapItem;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class News {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    @Column(columnDefinition = "TEXT")
    private String contents;

    private News(NewsBuilder builder) {
        this.title = builder.title;
        this.contents = builder.contents;
    }
    public static class NewsBuilder implements CommonModelBuilder<News> {
        private final String title;
        private final String contents;

        public NewsBuilder(ScrapItem scrapItem) {
            this.title = scrapItem.getTitle();
            this.contents = scrapItem.getContents();
        }
        @Override
        public News build() {
            return new News(this);
        }
    }
}
