package com.seop.newsscrap2.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScrapItem {
    private String title;
    private String contents;
}
