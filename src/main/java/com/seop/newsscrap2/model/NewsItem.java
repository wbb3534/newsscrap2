package com.seop.newsscrap2.model;

import com.seop.newsscrap2.entity.News;
import com.seop.newsscrap2.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class NewsItem {
    private Long id;
    private String title;
    private String contents;
    private LocalDate postNewsDate;

    private NewsItem(NewsItemBuilder builder) {
        this.id = builder.id;
        this.title = builder.title;
        this.contents = builder.contents;
        this.postNewsDate = builder.postNewsDate;

    }
    public static class NewsItemBuilder implements CommonModelBuilder<NewsItem> {
        private final Long id;
        private final String title;
        private final String contents;
        private final LocalDate postNewsDate;

        public NewsItemBuilder (News news) {
            this.id = news.getId();
            this.title = news.getTitle();
            this.contents = news.getContents();
            this.postNewsDate = LocalDate.now();
        }
        @Override
        public NewsItem build() {
            return new NewsItem(this);
        }
    }
}
