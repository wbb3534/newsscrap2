package com.seop.newsscrap2.service;

import com.seop.newsscrap2.entity.News;
import com.seop.newsscrap2.model.ScrapItem;
import com.seop.newsscrap2.repository.NewsRepository;
import lombok.RequiredArgsConstructor;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ScrapService {
    private final NewsRepository newsRepository;

    //뉴스사이트에서 긁어오고 디비에 등록하는 기능까지

    //뉴스사이트에서 전체 html을 긁어옴
    private Document getFullHtml() throws IOException {
        String URL = "https://www.kmib.co.kr/news/index.asp";
        Connection connection = Jsoup.connect(URL);

        return connection.get();
    }
    //뉴스사이트에서 rel_lst클래스에 해당하며 a태그를 가져옴
    private List<Element> parseElement(Document document) {
        Elements elements = document.getElementsByClass("rel_lst");
        List<Element> result = new LinkedList<>();

        /*for (Element item : elements) {
            Elements as = item.getElementsByTag("a");
            result.addAll(as);
        }*/
        elements.forEach(item -> result.addAll(item.getElementsByTag("a")));
        return result;
    }
    //a태그에서 href를 추출
    private List<String> parseHref(List<Element> list) {
        List<String> result = new LinkedList<>();

        /*for (Element item : list) {
            String href = item.attr("href");

            result.add(href);
        }*/
        list.forEach(item -> result.add(item.attr("href")));
        return result;
    }
    //디테일 뉴스 html을 긁어옴
    private List<Document> getDetailHtml(List<String> list) throws IOException {
        List<Document> result = new LinkedList<>();

        for (String item : list) {
            Connection connection = Jsoup.connect(item);
            Document document = connection.get();

            result.add(document);
        }
        return result;
    }
    //디테일 뉴스 html 에서 타이틀 본문 파싱
    private List<ScrapItem> parseHtml(List<Document> documents) {
        List<ScrapItem> result = new LinkedList<>();

        documents.forEach(item -> {
            String title = item.getElementsByClass("nwsti").get(0).getElementsByTag("h3").get(0).text();
            String contents = item.getElementsByClass("tx").get(0).text();

            ScrapItem addItem = new ScrapItem();
            addItem.setTitle(title);
            addItem.setContents(contents);

            result.add(addItem);
        });
        return result;
    }
    //디비에 등록
    public void setNews(List<ScrapItem> list) {
        for (ScrapItem item : list) {
            News addItem = new News.NewsBuilder(item).build();

            newsRepository.save(addItem);
        }
    }

    public List<ScrapItem> run() throws IOException {
        Document document = getFullHtml();
        List<Element> Element = parseElement(document);
        List<String> href = parseHref(Element);
        List<Document> detailDocument = getDetailHtml(href);
        List<ScrapItem> result = parseHtml(detailDocument);
        return result;
    }
}
