package com.seop.newsscrap2.service;

import com.seop.newsscrap2.entity.News;
import com.seop.newsscrap2.model.NewsItem;
import com.seop.newsscrap2.repository.NewsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class NewsService {
    private final NewsRepository newsRepository;

    public List<NewsItem> getNews() {
        List<News> originData = newsRepository.findAll();
        List<NewsItem> result = new LinkedList<>();

        /*for (News data : originData) {
            NewsItem addItem = new NewsItem.NewsItemBuilder(data).build();

            result.add(addItem);
        }*/
        originData.forEach(data -> result.add(new NewsItem.NewsItemBuilder(data).build()));
        return result;
    }
}
