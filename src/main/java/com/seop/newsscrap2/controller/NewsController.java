package com.seop.newsscrap2.controller;

import com.seop.newsscrap2.entity.News;
import com.seop.newsscrap2.model.NewsItem;
import com.seop.newsscrap2.model.ScrapItem;
import com.seop.newsscrap2.service.NewsService;
import com.seop.newsscrap2.service.ScrapService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RequestMapping("/v1/news")
@RestController
@RequiredArgsConstructor
public class NewsController {
    private final ScrapService scrapService;
    private final NewsService newsService;

    @GetMapping("/html") //타이틀 컨텐츠를 긁어옴
    public List<ScrapItem> getHtml() throws IOException {
        return scrapService.run();
    }
    @PostMapping("/news") //긁어온 데이터 디비에등록
    public String setNews() throws IOException {
        List<ScrapItem> result = scrapService.run(); //파싱한 결과물을 가져오는 코드
        scrapService.setNews(result);

        return "ok";
    }

    @GetMapping("/all")
    public List<NewsItem> getNewsData() {
        return newsService.getNews();
    }
}
