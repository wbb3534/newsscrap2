package com.seop.newsscrap2.repository;

import com.seop.newsscrap2.entity.News;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NewsRepository extends JpaRepository<News, Long> {
}
