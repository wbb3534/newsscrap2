package com.seop.newsscrap2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewsScrap2Application {

    public static void main(String[] args) {
        SpringApplication.run(NewsScrap2Application.class, args);
    }

}
